package pl.zai.service;


import pl.zai.controller.command.CreateAddressCommand;
import pl.zai.controller.command.UpdateAddressCommand;
import pl.zai.service.model.Address;

import java.util.List;

public interface AddressService {

    List<Address> get();

    Address get(Long id);

    void delete(Long id);

    Address edit(UpdateAddressCommand command, Long id);

    Address create(CreateAddressCommand command);
}
