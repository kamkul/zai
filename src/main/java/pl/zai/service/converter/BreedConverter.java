package pl.zai.service.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.command.CreateBreedCommand;
import pl.zai.controller.command.UpdateBreedCommand;
import pl.zai.service.model.Breed;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class BreedConverter {

    public List<Breed> convertTo(List<pl.zai.repository.model.Breed> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(BreedConverter::convert).collect(Collectors.toList()) : null;
    }

    public List<pl.zai.repository.model.Breed> convertFrom(List<Breed> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(BreedConverter::convert).collect(Collectors.toList()) : null;
    }

    public Breed convert(pl.zai.repository.model.Breed toConvert) {
        return Objects.nonNull(toConvert) ? Breed.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .build() : null;
    }

    public pl.zai.repository.model.Breed convert(Breed toConvert) {
        return Objects.nonNull(toConvert) ? pl.zai.repository.model.Breed.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .build() : null;
    }

    public Breed convert(CreateBreedCommand toConvert) {
        return Objects.nonNull(toConvert) ? Breed.builder()
                .name(toConvert.getBreedName())
                .build() : null;
    }

    public Breed convert(UpdateBreedCommand toConvert, Long id) {
        return Objects.nonNull(toConvert) ? Breed.builder()
                .id(id)
                .name(toConvert.getBreedName())
                .build() : null;
    }

}
