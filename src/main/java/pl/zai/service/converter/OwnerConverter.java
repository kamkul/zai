package pl.zai.service.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.command.CreateOwnerCommand;
import pl.zai.service.model.Address;
import pl.zai.service.model.Cat;
import pl.zai.service.model.Owner;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class OwnerConverter {

    public static List<Owner> convertTo(List<pl.zai.repository.model.Owner> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(OwnerConverter::convert).collect(Collectors.toList()) : null;
    }

    public static List<pl.zai.repository.model.Owner> convertFrom(List<Owner> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(OwnerConverter::convert).collect(Collectors.toList()) : null;
    }

    public Owner convert(pl.zai.repository.model.Owner toConvert) {
        return Objects.nonNull(toConvert) ? Owner.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .surname(toConvert.getSurname())
                .phone(toConvert.getPhone())
                .addresses(AddressConverter.convertTo(toConvert.getAddresses()))
                .cats(CatConverter.convertTo(toConvert.getCats()))
                .build() : null;
    }

    public pl.zai.repository.model.Owner convert(Owner toConvert) {
        return Objects.nonNull(toConvert) ? pl.zai.repository.model.Owner.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .surname(toConvert.getSurname())
                .phone(toConvert.getPhone())
                .addresses(AddressConverter.convertFrom(toConvert.getAddresses()))
                .cats(CatConverter.convertFrom(toConvert.getCats()))
                .build() : null;
    }

    public Owner convert(CreateOwnerCommand toConvert, List<Address> addresses, List<Cat> cats, Long id) {
        return Objects.nonNull(toConvert) ? Owner.builder()
                .id(id)
                .name(toConvert.getName())
                .surname(toConvert.getSurname())
                .phone(toConvert.getPhone())
                .addresses(addresses)
                .cats(cats)
                .build() : null;
    }

}
