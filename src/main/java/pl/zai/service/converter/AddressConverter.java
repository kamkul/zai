package pl.zai.service.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.command.CreateAddressCommand;
import pl.zai.controller.command.UpdateAddressCommand;
import pl.zai.service.model.Address;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class AddressConverter {

    public static List<Address> convertTo(List<pl.zai.repository.model.Address> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(AddressConverter::convert).collect(Collectors.toList()) : null;
    }

    public static List<pl.zai.repository.model.Address> convertFrom(List<Address> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(AddressConverter::convert).collect(Collectors.toList()) : null;
    }

    public static Address convert(pl.zai.repository.model.Address toConvert) {
        return Objects.nonNull(toConvert) ? Address.builder()
                .address(toConvert.getAddress())
                .id(toConvert.getId())
                .city(toConvert.getCity())
                .postalCode(toConvert.getPostalCode())
                .build() : null;
    }

    public static pl.zai.repository.model.Address convert(Address toConvert) {
        return Objects.nonNull(toConvert) ? pl.zai.repository.model.Address.builder()
                .address(toConvert.getAddress())
                .id(toConvert.getId())
                .city(toConvert.getCity())
                .postalCode(toConvert.getPostalCode())
                .build() : null;
    }

    public static Address convert(CreateAddressCommand toConvert) {
        return Address.builder()
                .address(toConvert.getAddress())
                .city(toConvert.getCity())
                .postalCode(toConvert.getPostalCode())
                .build();
    }

    public static Address convert(UpdateAddressCommand toConvert, Long id) {
        return Address.builder()
                .address(toConvert.getAddress())
                .city(toConvert.getCity())
                .postalCode(toConvert.getPostalCode())
                .id(id)
                .build();
    }

}
