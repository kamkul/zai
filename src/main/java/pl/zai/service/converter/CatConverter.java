package pl.zai.service.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.command.CreateCatCommand;
import pl.zai.controller.command.UpdateCatCommand;
import pl.zai.service.model.Breed;
import pl.zai.service.model.Cat;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class CatConverter {

    public static List<Cat> convertTo(List<pl.zai.repository.model.Cat> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(CatConverter::convert).collect(Collectors.toList()) : null;
    }

    public static List<pl.zai.repository.model.Cat> convertFrom(List<Cat> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(CatConverter::convert).collect(Collectors.toList()) : null;
    }

    public Cat convert(pl.zai.repository.model.Cat toConvert) {
        return Objects.nonNull(toConvert) ? Cat.builder()
                .birthDate(toConvert.getBirthDate())
                .id(toConvert.getId())
                .name(toConvert.getName())
                .breed(BreedConverter.convert(toConvert.getBreed()))
                .build() : null;
    }

    public pl.zai.repository.model.Cat convert(Cat toConvert) {
        return Objects.nonNull(toConvert) ? pl.zai.repository.model.Cat.builder()
                .birthDate(toConvert.getBirthDate())
                .id(toConvert.getId())
                .name(toConvert.getName())
                .breed(BreedConverter.convert(toConvert.getBreed()))
                .build() : null;
    }

    public Cat convert(CreateCatCommand toConvert, Breed breed) {
        return Objects.nonNull(toConvert) ? Cat.builder()
                .birthDate(toConvert.getBirthDate())
                .name(toConvert.getName())
                .breed(breed)
                .build() : null;
    }

    public Cat convert(UpdateCatCommand toConvert, Breed breed, Long id) {
        return Objects.nonNull(toConvert) ? Cat.builder()
                .birthDate(toConvert.getBirthDate())
                .name(toConvert.getName())
                .breed(breed)
                .id(id)
                .build() : null;
    }

}
