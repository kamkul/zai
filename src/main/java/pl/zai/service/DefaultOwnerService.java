package pl.zai.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.zai.controller.command.CreateOwnerCommand;
import pl.zai.controller.command.UpdateOwnerCommand;
import pl.zai.repository.AddressRepository;
import pl.zai.repository.CatRepository;
import pl.zai.repository.OwnerRepository;
import pl.zai.service.converter.OwnerConverter;
import pl.zai.service.model.Owner;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultOwnerService implements OwnerService {

    private final OwnerRepository repository;
    private final CatRepository catRepository;
    private final AddressRepository addressRepository;

    @Override
    public List<Owner> get() {
        return OwnerConverter.convertTo(repository.findAll());
    }

    @Override
    public Owner get(Long id) {
        pl.zai.repository.model.Owner owner = repository.findById(id).orElseThrow(RuntimeException::new);
        List<pl.zai.repository.model.Cat> cats = catRepository.getByOwnerId(id);
        List<pl.zai.repository.model.Address> adresses = addressRepository.getByOwnerId(id);
        owner.setCats(cats);
        owner.setAddresses(adresses);

        return OwnerConverter.convert(owner);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Owner edit(UpdateOwnerCommand command, Long id) {
        pl.zai.repository.model.Owner retrievedOwner = repository.findById(id).orElseThrow(RuntimeException::new);
        retrievedOwner.setName(command.getName());
        retrievedOwner.setPhone(command.getPhone());
        retrievedOwner.setSurname(command.getSurname());

        return OwnerConverter.convert(repository.save(retrievedOwner));
    }

    @Override
    public Owner create(CreateOwnerCommand command) {
        pl.zai.repository.model.Owner newOwner = pl.zai.repository.model.Owner.builder()
                .phone(command.getPhone())
                .surname(command.getSurname())
                .name(command.getName())
                .build();

        return OwnerConverter.convert(repository.save(newOwner));
    }

}
