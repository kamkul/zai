package pl.zai.service;

import pl.zai.controller.command.CreateCatCommand;
import pl.zai.controller.command.UpdateCatCommand;
import pl.zai.service.model.Cat;

import java.util.List;

public interface CatService {

    List<Cat> get();

    Cat get(Long id);

    void delete(Long id);

    Cat edit(UpdateCatCommand command, Long id);

    Cat create(CreateCatCommand command);

}
