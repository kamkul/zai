package pl.zai.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.zai.controller.command.CreateAddressCommand;
import pl.zai.controller.command.UpdateAddressCommand;
import pl.zai.repository.AddressRepository;
import pl.zai.repository.OwnerRepository;
import pl.zai.service.converter.AddressConverter;
import pl.zai.service.model.Address;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultAddressService implements AddressService {

    private final AddressRepository repository;
    private final OwnerRepository ownerRepository;

    @Override
    public List<Address> get() {
        return AddressConverter.convertTo(repository.findAll());
    }

    @Override
    public Address get(Long id) {
        return AddressConverter.convert(repository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Address edit(UpdateAddressCommand command, Long id) {
        pl.zai.repository.model.Address retrievedAddress = repository.getById(id);
        retrievedAddress.setAddress(command.getAddress());
        retrievedAddress.setCity(command.getCity());
        retrievedAddress.setPostalCode(command.getPostalCode());

        pl.zai.repository.model.Owner retrievedOwner = ownerRepository.getById(command.getOwnerId());
        retrievedAddress.setOwner(retrievedOwner);

        return AddressConverter.convert(repository.save(retrievedAddress));
    }

    @Override
    public Address create(CreateAddressCommand command) {
        pl.zai.repository.model.Owner retrievedOwner = ownerRepository.getById(command.getOwnerId());

        pl.zai.repository.model.Address newAddress = pl.zai.repository.model.Address.builder()
                .city(command.getCity())
                .address(command.getAddress())
                .postalCode(command.getPostalCode())
                .owner(retrievedOwner)
                .build();

        return AddressConverter.convert(repository.save(newAddress));
    }

}
