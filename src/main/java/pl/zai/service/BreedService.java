package pl.zai.service;


import pl.zai.controller.command.CreateBreedCommand;
import pl.zai.controller.command.UpdateBreedCommand;
import pl.zai.service.model.Breed;

import java.util.List;

public interface BreedService {

    List<Breed> get();

    Breed get(Long id);

    void delete(Long id);

    Breed edit(UpdateBreedCommand command, Long id);

    Breed create(CreateBreedCommand command);

}
