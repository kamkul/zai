package pl.zai.service;

import pl.zai.controller.command.CreateOwnerCommand;
import pl.zai.controller.command.UpdateOwnerCommand;
import pl.zai.service.model.Owner;

import java.util.List;

public interface OwnerService {

    List<Owner> get();

    Owner get(Long id);

    void delete(Long id);

    Owner edit(UpdateOwnerCommand command, Long id);

    Owner create(CreateOwnerCommand command);

}
