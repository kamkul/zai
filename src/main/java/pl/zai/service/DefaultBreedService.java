package pl.zai.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.zai.controller.command.CreateBreedCommand;
import pl.zai.controller.command.UpdateBreedCommand;
import pl.zai.repository.BreedRepository;
import pl.zai.service.converter.BreedConverter;
import pl.zai.service.model.Breed;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultBreedService implements BreedService {

    private final BreedRepository repository;

    @Override
    public List<Breed> get() {
        return BreedConverter.convertTo(repository.findAll());
    }

    @Override
    public Breed get(Long id) {
        return BreedConverter.convert(repository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Breed edit(UpdateBreedCommand command, Long id) {
        pl.zai.repository.model.Breed retrievedBreed = repository.findById(id).orElseThrow(RuntimeException::new);
        retrievedBreed.setName(command.getBreedName());

        return BreedConverter.convert(repository.save(retrievedBreed));
    }

    @Override
    public Breed create(CreateBreedCommand command) {
        pl.zai.repository.model.Breed newBreed = pl.zai.repository.model.Breed.builder()
                .name(command.getBreedName())
                .build();

        return BreedConverter.convert(repository.save(newBreed));
    }

}
