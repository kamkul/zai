package pl.zai.service.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cat {

    private Long id;
    private String name;
    private LocalDate birthDate;
    private Owner owner;
    private Breed breed;

}
