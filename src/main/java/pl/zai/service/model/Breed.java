package pl.zai.service.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Breed {

    private Long id;
    private String name;
    private List<Cat> cats = new ArrayList<>();

}
