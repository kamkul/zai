package pl.zai.service.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private Long id;
    private String city;
    private String address;
    private String postalCode;
    private Owner owner;

}
