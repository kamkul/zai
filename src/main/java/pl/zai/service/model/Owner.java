package pl.zai.service.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Owner {

    private Long id;
    private String name;
    private String surname;
    private String phone;
    private List<Cat> cats = new ArrayList<>();
    private List<Address> addresses = new ArrayList<>();

}
