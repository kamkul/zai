package pl.zai.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.zai.controller.command.CreateCatCommand;
import pl.zai.controller.command.UpdateCatCommand;
import pl.zai.repository.BreedRepository;
import pl.zai.repository.CatRepository;
import pl.zai.repository.OwnerRepository;
import pl.zai.service.converter.CatConverter;
import pl.zai.service.model.Cat;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultCatService implements CatService {

    private final CatRepository repository;
    private final BreedRepository breedRepository;
    private final OwnerRepository ownerRepository;

    @Override
    public List<Cat> get() {
        return CatConverter.convertTo(repository.findAll());
    }

    @Override
    public Cat get(Long id) {
        return CatConverter.convert(repository.findById(id).orElseThrow(RuntimeException::new));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Cat edit(UpdateCatCommand command, Long id) {
        pl.zai.repository.model.Breed retrievedBreed = breedRepository.findById(command.getBreedId()).orElseThrow(RuntimeException::new);
        pl.zai.repository.model.Owner retrievedOwner = ownerRepository.findById(command.getOwnerId()).orElseThrow(RuntimeException::new);

        pl.zai.repository.model.Cat retrievedCat = repository.findById(id).orElseThrow(RuntimeException::new);
        retrievedCat.setBreed(retrievedBreed);
        retrievedCat.setOwner(retrievedOwner);
        retrievedCat.setBirthDate(command.getBirthDate());
        retrievedCat.setName(command.getName());

        return CatConverter.convert(repository.save(retrievedCat));
    }

    @Override
    public Cat create(CreateCatCommand command) {
        pl.zai.repository.model.Breed retrievedBreed = breedRepository.findById(command.getBreedId()).orElseThrow(RuntimeException::new);
        pl.zai.repository.model.Owner retrievedOwner = ownerRepository.findById(command.getOwnerId()).orElseThrow(RuntimeException::new);

        pl.zai.repository.model.Cat newCat = pl.zai.repository.model.Cat.builder()
                .name(command.getName())
                .birthDate(command.getBirthDate())
                .owner(retrievedOwner)
                .breed(retrievedBreed)
                .build();

        return CatConverter.convert(repository.save(newCat));
    }

}
