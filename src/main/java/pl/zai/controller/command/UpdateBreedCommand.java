package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UpdateBreedCommand {

    private final String breedName;

}
