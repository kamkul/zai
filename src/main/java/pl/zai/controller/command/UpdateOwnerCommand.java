package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
public class UpdateOwnerCommand {

    private final String name;
    private final String surname;
    private final String phone;

}
