package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;
import pl.zai.controller.model.BreedDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
public class CreateCatCommand {

    private final String name;
    private final LocalDate birthDate;
    private final Long ownerId;
    private final Long breedId;

}
