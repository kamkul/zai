package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CreateBreedCommand {

    private final String breedName;

}
