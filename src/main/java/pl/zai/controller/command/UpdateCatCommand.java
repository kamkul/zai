package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
public class UpdateCatCommand {

    private final String name;
    private final LocalDate birthDate;
    private final Long breedId;
    private final Long ownerId;

}
