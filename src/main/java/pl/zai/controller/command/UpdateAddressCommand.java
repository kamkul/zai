package pl.zai.controller.command;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UpdateAddressCommand {

    private final String city;
    private final String address;
    private final String postalCode;
    private final Long ownerId;

}
