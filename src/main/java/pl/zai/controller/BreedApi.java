package pl.zai.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zai.controller.command.CreateBreedCommand;
import pl.zai.controller.command.UpdateBreedCommand;
import pl.zai.controller.model.BreedDTO;

import java.util.List;

public interface BreedApi {

    @GetMapping(path = "/api/breed")
    ResponseEntity<List<BreedDTO>> getAll();

    @GetMapping(path = "/api/breed/{breedId}")
    ResponseEntity<BreedDTO> getById(@PathVariable("breedId") Long id);

    @PostMapping(path = "/api/breed")
    ResponseEntity<BreedDTO> create(@RequestBody CreateBreedCommand command);

    @DeleteMapping(path = "/api/breed/{breedId}")
    ResponseEntity<Void> delete(@PathVariable("breedId") Long id);

    @PatchMapping(path = "/api/breed/{breedId}")
    ResponseEntity<BreedDTO> edit(@RequestBody UpdateBreedCommand command, @PathVariable("breedId") Long id);

}
