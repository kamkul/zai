package pl.zai.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

public interface UserApi {

    @GetMapping(path = "/user/cats")
    String viewCats(Model model);

}
