package pl.zai.controller.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AddressDTO {

    private final Long id;
    private final String city;
    private final String address;
    private final String postalCode;

}
