package pl.zai.controller.model;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
public class OwnerDTO {

    private final Long id;
    private final String name;
    private final String surname;
    private final String phone;
    private final List<CatDTO> cats;
    private final List<AddressDTO> addresses;

}
