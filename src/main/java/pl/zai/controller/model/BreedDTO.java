package pl.zai.controller.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class BreedDTO {

    private final Long id;
    private final String name;

}
