package pl.zai.controller.model;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
public class CatDTO {

    private final Long id;
    private final String name;
    private final LocalDate birthDate;
    private final BreedDTO breed;

}
