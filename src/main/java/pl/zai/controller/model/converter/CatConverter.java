package pl.zai.controller.model.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.model.AddressDTO;
import pl.zai.controller.model.CatDTO;
import pl.zai.service.model.Cat;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class CatConverter {

    public static List<CatDTO> convert(List<pl.zai.service.model.Cat> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(CatConverter::convert).collect(Collectors.toList()) : null;
    }

    public CatDTO convert(Cat toConvert) {
        return Objects.nonNull(toConvert) ? CatDTO.builder()
                .birthDate(toConvert.getBirthDate())
                .id(toConvert.getId())
                .name(toConvert.getName())
                .breed(BreedConverter.convert(toConvert.getBreed()))
                .build() : null;
    }

}
