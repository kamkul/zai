package pl.zai.controller.model.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.model.AddressDTO;
import pl.zai.service.model.Address;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class AddressConverter {

    public static List<AddressDTO> convert(List<pl.zai.service.model.Address> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(AddressConverter::convert).collect(Collectors.toList()) : null;
    }

    public static AddressDTO convert(Address toConvert) {
        return Objects.nonNull(toConvert) ? AddressDTO.builder()
                .address(toConvert.getAddress())
                .id(toConvert.getId())
                .city(toConvert.getCity())
                .postalCode(toConvert.getPostalCode())
                .build() : null;
    }

}
