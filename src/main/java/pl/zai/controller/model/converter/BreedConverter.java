package pl.zai.controller.model.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.model.BreedDTO;
import pl.zai.service.model.Breed;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class BreedConverter {

    public static List<BreedDTO> convert(List<pl.zai.service.model.Breed> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(BreedConverter::convert).collect(Collectors.toList()) : null;
    }

    public BreedDTO convert(Breed toConvert) {
        return Objects.nonNull(toConvert) ? BreedDTO.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .build() : null;
    }

}
