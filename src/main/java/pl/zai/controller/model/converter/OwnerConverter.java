package pl.zai.controller.model.converter;

import lombok.experimental.UtilityClass;
import pl.zai.controller.model.OwnerDTO;
import pl.zai.service.model.Owner;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class OwnerConverter {

    public static List<OwnerDTO> convert(List<pl.zai.service.model.Owner> toConvert) {
        return Objects.nonNull(toConvert) ? toConvert.stream().map(OwnerConverter::convert).collect(Collectors.toList()) : null;
    }

    public OwnerDTO convert(Owner toConvert) {
        return Objects.nonNull(toConvert) ? OwnerDTO.builder()
                .id(toConvert.getId())
                .name(toConvert.getName())
                .surname(toConvert.getSurname())
                .phone(toConvert.getPhone())
                .addresses(AddressConverter.convert(toConvert.getAddresses()))
                .cats(CatConverter.convert(toConvert.getCats()))
                .build() : null;
    }

}
