package pl.zai.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

public interface AdminApi {

    @GetMapping(path = "/admin/owners")
    String viewOwners(Model model);

}
