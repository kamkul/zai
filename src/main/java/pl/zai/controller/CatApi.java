package pl.zai.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zai.controller.command.CreateCatCommand;
import pl.zai.controller.command.UpdateCatCommand;
import pl.zai.controller.model.CatDTO;

import java.util.List;

public interface CatApi {

    @GetMapping(path = "/api/cat")
    ResponseEntity<List<CatDTO>> getAll();

    @GetMapping(path = "/api/cat/{catId}")
    ResponseEntity<CatDTO> getById(@PathVariable("catId") Long id);

    @PostMapping(path = "/api/cat")
    ResponseEntity<CatDTO> create(@RequestBody CreateCatCommand command);

    @DeleteMapping(path = "/api/cat/{catId}")
    ResponseEntity<Void> delete(@PathVariable("catId") Long id);

    @PatchMapping(path = "/api/cat/{catId}")
    ResponseEntity<CatDTO> edit(@RequestBody UpdateCatCommand command, @PathVariable("catId") Long id);

}
