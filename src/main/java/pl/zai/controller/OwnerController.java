package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import pl.zai.controller.command.CreateOwnerCommand;
import pl.zai.controller.command.UpdateOwnerCommand;
import pl.zai.controller.model.OwnerDTO;
import pl.zai.controller.model.converter.OwnerConverter;
import pl.zai.service.OwnerService;

import java.util.List;

@Controller
@AllArgsConstructor
public class OwnerController implements OwnerApi {

    private final OwnerService service;

    @Override
    public ResponseEntity<List<OwnerDTO>> getAll() {
        return ResponseEntity.ok(OwnerConverter.convert(service.get()));
    }

    @Override
    public ResponseEntity<OwnerDTO> getById(Long id) {
        return ResponseEntity.ok(OwnerConverter.convert(service.get(id)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<OwnerDTO> create(CreateOwnerCommand command) {
        return ResponseEntity.ok(OwnerConverter.convert(service.create(command)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<OwnerDTO> edit(UpdateOwnerCommand command, Long id) {
        return ResponseEntity.ok(OwnerConverter.convert(service.edit(command, id)));
    }


}
