package pl.zai.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zai.controller.command.CreateOwnerCommand;
import pl.zai.controller.command.UpdateOwnerCommand;
import pl.zai.controller.model.OwnerDTO;

import java.util.List;

public interface OwnerApi {

    @GetMapping(path = "/api/owner")
    ResponseEntity<List<OwnerDTO>> getAll();

    @GetMapping(path = "/api/owner/{ownerId}")
    ResponseEntity<OwnerDTO> getById(@PathVariable("ownerId") Long id);

    @PostMapping(path = "/api/owner")
    ResponseEntity<OwnerDTO> create(@RequestBody CreateOwnerCommand command);

    @DeleteMapping(path = "/api/owner/{ownerId}")
    ResponseEntity<Void> delete(@PathVariable("ownerId") Long id);

    @PatchMapping(path = "/api/owner/{ownerId}")
    ResponseEntity<OwnerDTO> edit(@RequestBody UpdateOwnerCommand command, @PathVariable("ownerId") Long id);
}
