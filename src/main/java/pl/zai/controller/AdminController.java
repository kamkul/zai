package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import pl.zai.controller.model.OwnerDTO;
import pl.zai.controller.model.converter.OwnerConverter;
import pl.zai.service.OwnerService;

import java.util.List;

@Controller
@AllArgsConstructor
public class AdminController implements AdminApi {

    private final OwnerService ownerService;

    @Override
    @PreAuthorize("hasRole('admin')")
    public String viewOwners(Model model) {
        List<OwnerDTO> owners = OwnerConverter.convert(ownerService.get());
        model.addAttribute("owners", owners);

        return "owners";
    }
}
