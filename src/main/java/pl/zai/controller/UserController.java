package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import pl.zai.controller.model.CatDTO;
import pl.zai.controller.model.OwnerDTO;
import pl.zai.controller.model.converter.CatConverter;
import pl.zai.controller.model.converter.OwnerConverter;
import pl.zai.service.CatService;

import java.util.List;

@Controller
@AllArgsConstructor
public class UserController implements UserApi {

    private final CatService catService;

    @Override
    public String viewCats(Model model) {
        List<CatDTO> cats = CatConverter.convert(catService.get());
        model.addAttribute("cats", cats);

        return "cats";
    }
}
