package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import pl.zai.controller.command.CreateAddressCommand;
import pl.zai.controller.command.UpdateAddressCommand;
import pl.zai.controller.model.AddressDTO;
import pl.zai.controller.model.converter.AddressConverter;
import pl.zai.service.AddressService;

import java.util.List;

@Controller
@AllArgsConstructor
public class AddressController implements AddressApi {

    private final AddressService service;

    @Override
    public ResponseEntity<List<AddressDTO>> getAll() {
        return ResponseEntity.ok(AddressConverter.convert(service.get()));
    }

    @Override
    public ResponseEntity<AddressDTO> getById(Long id) {
        return ResponseEntity.ok(AddressConverter.convert(service.get(id)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<AddressDTO> create(CreateAddressCommand command) {
        return ResponseEntity.ok(AddressConverter.convert(service.create(command)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<AddressDTO> edit(UpdateAddressCommand command, Long id) {
        return ResponseEntity.ok(AddressConverter.convert(service.edit(command, id)));
    }
}
