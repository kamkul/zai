package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import pl.zai.controller.command.CreateBreedCommand;
import pl.zai.controller.command.UpdateBreedCommand;
import pl.zai.controller.model.BreedDTO;
import pl.zai.controller.model.converter.BreedConverter;
import pl.zai.service.BreedService;

import java.util.List;

@Controller
@AllArgsConstructor
public class BreedController implements BreedApi {

    private final BreedService service;

    @Override
    public ResponseEntity<List<BreedDTO>> getAll() {
        return ResponseEntity.ok(BreedConverter.convert(service.get()));
    }

    @Override
    public ResponseEntity<BreedDTO> getById(Long id) {
        return ResponseEntity.ok(BreedConverter.convert(service.get(id)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<BreedDTO> create(CreateBreedCommand command) {
        return ResponseEntity.ok(BreedConverter.convert(service.create(command)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<BreedDTO> edit(UpdateBreedCommand command, Long id) {
        return ResponseEntity.ok(BreedConverter.convert(service.edit(command, id)));
    }


}
