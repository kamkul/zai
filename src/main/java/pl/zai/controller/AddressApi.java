package pl.zai.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zai.controller.command.CreateAddressCommand;
import pl.zai.controller.command.UpdateAddressCommand;
import pl.zai.controller.model.AddressDTO;

import java.util.List;

public interface AddressApi {

    @GetMapping(path = "/api/address")
    ResponseEntity<List<AddressDTO>> getAll();

    @GetMapping(path = "/api/address/{addressId}")
    ResponseEntity<AddressDTO> getById(@PathVariable("addressId") Long id);

    @PostMapping(path = "/api/address")
    ResponseEntity<AddressDTO> create(@RequestBody CreateAddressCommand command);

    @DeleteMapping(path = "/api/address/{addressId}")
    ResponseEntity<Void> delete(@PathVariable("addressId") Long id);

    @PatchMapping(path = "/api/address/{addressId}")
    ResponseEntity<AddressDTO> edit(@RequestBody UpdateAddressCommand command, @PathVariable("addressId") Long id);

}
