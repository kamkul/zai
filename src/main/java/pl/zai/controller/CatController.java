package pl.zai.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import pl.zai.controller.command.CreateCatCommand;
import pl.zai.controller.command.UpdateCatCommand;
import pl.zai.controller.model.CatDTO;
import pl.zai.controller.model.converter.CatConverter;
import pl.zai.service.CatService;

import java.util.List;

@Controller
@AllArgsConstructor
public class CatController implements CatApi {

    private final CatService service;

    @Override
    public ResponseEntity<List<CatDTO>> getAll() {
        return ResponseEntity.ok(CatConverter.convert(service.get()));
    }

    @Override
    public ResponseEntity<CatDTO> getById(Long id) {
        return ResponseEntity.ok(CatConverter.convert(service.get(id)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<CatDTO> create(CreateCatCommand command) {
        return ResponseEntity.ok(CatConverter.convert(service.create(command)));
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<CatDTO> edit(UpdateCatCommand command, Long id) {
        return ResponseEntity.ok(CatConverter.convert(service.edit(command, id)));
    }

}
