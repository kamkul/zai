package pl.zai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.zai.repository.model.Cat;

import java.util.List;

@Repository
public interface CatRepository extends JpaRepository<Cat, Long> {

    @Query("SELECT c FROM Cat c WHERE c.owner.id = ?1")
    List<Cat> getByOwnerId(Long ownerId);

}
