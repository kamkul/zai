package pl.zai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.zai.repository.model.Address;
import pl.zai.repository.model.Cat;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT a FROM Address a WHERE a.owner.id = ?1")
    List<Address> getByOwnerId(Long ownerId);

}
