package pl.zai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zai.repository.model.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {
}
