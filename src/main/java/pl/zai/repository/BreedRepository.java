package pl.zai.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zai.repository.model.Breed;

@Repository
public interface BreedRepository extends JpaRepository<Breed, Long> {
}
