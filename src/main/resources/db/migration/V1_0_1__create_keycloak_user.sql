USE keycloak;

CREATE LOGIN keycloak_user WITH PASSWORD = 'keycloak_P@ssword1';
CREATE USER keycloak_user FOR LOGIN keycloak_user;
ALTER ROLE db_owner ADD MEMBER keycloak_user;
EXEC sp_addrolemember N'db_datareader', N'keycloak_user';
EXEC sp_addrolemember N'db_datawriter', N'keycloak_user';
