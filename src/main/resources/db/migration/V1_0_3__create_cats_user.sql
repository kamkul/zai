USE cats;

CREATE LOGIN cats_user WITH PASSWORD = 'cats_P@ssword1';
CREATE USER cats_user FOR LOGIN cats_user;
ALTER ROLE db_owner ADD MEMBER cats_user;
EXEC sp_addrolemember N'db_datareader', N'cats_user';
EXEC sp_addrolemember N'db_datawriter', N'cats_user';
