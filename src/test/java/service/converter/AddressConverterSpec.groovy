package service.converter


import pl.zai.service.converter.AddressConverter
import pl.zai.service.model.Address
import spock.lang.Specification

class AddressConverterSpec extends Specification {

    def 'should convert repository list'() {
        given:
        def toConvert = [
                Address.builder()
                        .address("address")
                        .city("city")
                        .id(1L)
                        .build()
        ]

        when:
        def result = AddressConverter.convertFrom(toConvert)

        then:
        result != null
        result[0].id == toConvert[0].id
        result[0].address == toConvert[0].address
        result[0].city == toConvert[0].city
    }

}
