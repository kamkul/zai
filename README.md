<h1>Projekt</h1>
<h2>Stack</h2>
<p>Backend zostanie zrealizowany w SpringBoot</p>

<h2>Model danych</h2>
![db_schema](db_schema.png "Schemat bazy danych")

<h2>Jak uruchomić?</h2>
Potrzebujemy dockera
budujemy komendą
`./gradlew clean build`
<br>
Przechodzimy do /src/docker i odkomentowujemy linijkę 
`KEYCLOAK_IMPORT: /opt/jboss/keycloak/imports/realm-export.json `
,<br>
a następnie wykonujemy 
`docker-compose up --build`
w celu zaimportowania konfiguracji początkowej
<br>
Kolejne uruchomienia trzeba mieć zakomentowane powyższą linijkę
